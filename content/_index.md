---
####################### Banner #########################
banner:
  title : "Unisciti al progetto di liberazione della nostra didattica"
  image : "images/banner-art.svg"
  content : "Purtroppo in italia troppo docenti usano piattaforme di gafam. Unisciti alla resistenza"
  button:
    enable : true
    label : "Contattaci per unirti alla sperimentazione"
    link : "mailto:pizza@example.org"

##################### Feature ##########################
feature:
  enable : true
  title : "Devi sapere che..."
  feature_item:
    # feature item loop
    - name : "Questioni geopolitiche"
      icon : "fas fa-code"
      content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      
    # feature item loop
    - name : "No software libero"
      icon : "fas fa-object-group"
      content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      
    # feature item loop
    - name : "Controllo da parte di altri"
      icon : "fas fa-user-clock"
      content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      
    # # feature item loop
    # - name : "Value For Money"
    #   icon : "fas fa-heart"
    #   content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      
    # # feature item loop
    # - name : "Faster Response"
    #   icon : "fas fa-tachometer-alt"
    #   content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      
    # # feature item loop
    # - name : "Cloud Support"
    #   icon : "fas fa-cloud"
    #   content : "Lorem ipsum dolor sit amet consectetur adipisicing elit quam nihil"
      


######################### Service #####################
service:
  enable : true
  service_item:
    # service item loop
    - title : "Il progetto richiede nell'usare big blue button e blablabla"
      images:
      - "images/service-1.png"
      - "images/service-2.png"
      - "images/service-3.png"
      content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat. consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat."
      button:
        enable : false
        label : "Check it out"
        link : "#"
        
        
################### Screenshot ########################
screenshot:
  enable : true
  title : "Che la liberazione sia in te"
  image : "images/screenshot.svg"

  

##################### Call to action #####################
call_to_action:
  enable : true
  title : "Convinto? Unisciti alla resistenza"
  image : "images/cta.svg"
  content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur."
  button:
    enable : true
    label : "Contattaci per unirti alla sperimentazione"
    link : "mailto:pizza@example.org"

---